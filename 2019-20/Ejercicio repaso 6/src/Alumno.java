import java.time.LocalDate;
import java.util.Date;

public class Alumno extends Cole{
	
	String DNI;
	String Nombre_Apellidos;
	String Fecha_nacimiento;
	char sexo;
	boolean repetidor;
	
	public Alumno(String DNI, String nombre_Apellidos, String fecha_nacimiento, char sexo,
			boolean repetidor, String nombre_modulo) {
		super(nombre_modulo);
		this.DNI = DNI;
		this.Nombre_Apellidos = nombre_Apellidos;
		this.Fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getNombre_Apellidos() {
		return Nombre_Apellidos;
	}

	public void setNombre_Apellidos(String nombre_Apellidos) {
		Nombre_Apellidos = nombre_Apellidos;
	}

	public String getFecha_nacimiento() {
		return Fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		Fecha_nacimiento = fecha_nacimiento;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	@Override
	public String toString() {
		return "Alumno [DNI=" + DNI + ", Nombre_Apellidos=" + Nombre_Apellidos + ", Fecha_nacimiento="
				+ Fecha_nacimiento + ", sexo=" + sexo + ", repetidor=" + repetidor + ", nombre_modulo=" + nombre_modulo
				+ "]";
	}
	

}
