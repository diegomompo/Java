
public class Modulo extends Cole {
	
	int n_horas;
	String profesor;
	boolean convalidable;
	
	public Modulo(String nombre_modulo, int n_horas, String profesor, boolean convalidable) {
		super(nombre_modulo);
		this.n_horas = n_horas;
		this.profesor = profesor;
		this.convalidable = convalidable;
	}

	public int getN_horas() {
		return n_horas;
	}

	public void setN_horas(int n_horas) {
		this.n_horas = n_horas;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public boolean isConvalidable() {
		return convalidable;
	}

	public void setConvalidable(boolean convalidable) {
		this.convalidable = convalidable;
	}

	@Override
	public String toString() {
		return "Modulo [nombre_modulo=" + nombre_modulo + ", n_horas=" + n_horas + ", profesor=" + profesor
				+ ", convalidable=" + convalidable + "]";
	}
	
	

}
