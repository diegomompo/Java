import java.awt.EventQueue;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class Leccion {

	protected static final String Leccion1 = null;
	private JFrame PadreLeccion;
	private JFrame HijaLeccion;
	MenuElegir menuelegir;
	
	Teclado teclado = new Teclado();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Leccion window = new Leccion();
					window.PadreLeccion.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Leccion() {
		initialize();
		this.menuelegir = menuelegir;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		PadreLeccion = new JFrame();
		PadreLeccion.setBounds(100, 100, 450, 300);
		PadreLeccion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PadreLeccion.getContentPane().setLayout(null);
		
		JButton Leccion1 = new JButton("Lecci\u00F3n 1");
		Leccion1.setBounds(138, 53, 125, 38);
		PadreLeccion.getContentPane().add(Leccion1);
		
		Leccion1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				PadreLeccion();
				
			}
		
		});
		
		
		JButton Leccion2 = new JButton("Lecci\u00F3n 2");
		Leccion2.setBounds(138, 132, 125, 38);
		PadreLeccion.getContentPane().add(Leccion2);
		

		Leccion2.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				PadreLeccion2();
			}
		
		});

		JMenuBar menuAcercaDe = new JMenuBar();
		menuAcercaDe.setToolTipText("Acerca de");
		menuAcercaDe.setBounds(10, 11, 107, 21);
		PadreLeccion.getContentPane().add(menuAcercaDe);
		
		JMenu mnAcercaDe = new JMenu("Acerca de");
		menuAcercaDe.add(mnAcercaDe);
		
		JLabel lblEsteProgramaEst = new JLabel("Este programa est\u00E1 realizado por Diego Momp\u00F3 Redoli el 11 de octubre de 2019 ");
		mnAcercaDe.add(lblEsteProgramaEst);
	}
	
	public void HijaLeccion(MenuElegir menuelegir) {
		menuelegir.getFrame().setVisible(true);
		HijaLeccion.dispose();
	}
	
	public void PadreLeccion() {
		teclado.getFrame().setVisible(true);
		PadreLeccion.dispose();
		teclado.MostrarLeccion();
	}
	public void PadreLeccion2() {
		teclado.getFrame().setVisible(true);
		PadreLeccion.dispose();
		teclado.MostrarLeccion2();
	}


	public Window getFrame() {
		// TODO Auto-generated method stub
		return PadreLeccion;
	}
	public void setFrame(JFrame frame) {
		this.PadreLeccion = frame;
	}
}
