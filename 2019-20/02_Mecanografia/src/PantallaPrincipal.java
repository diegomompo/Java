import java.awt.EventQueue;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Time;
import java.util.Timer;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class PantallaPrincipal extends JFrame {

	private JFrame PantallaPrincipal;
	private JProgressBar progressBar;
	int segundos;
	int num = 0;
	
	public JFrame getFrame() {
		return  PantallaPrincipal;
	}

	public void setFrame(JFrame frame) {
		this. PantallaPrincipal = frame;
	}
	
	Login login = new Login(this);
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPrincipal window = new PantallaPrincipal();
					window. PantallaPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		/*PantallaPrincipal frame = new PantallaPrincipal();
		  frame.pack();
		  frame.setVisible(true);
		  frame.iterate();*/
	}

	/**
	 * Create the application.
	 * @return 
	 */
	public PantallaPrincipal() {
		initialize();

		}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		PantallaPrincipal = new JFrame();
		PantallaPrincipal.setBounds(100, 100, 450, 300);
		PantallaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PantallaPrincipal.getContentPane().setLayout(null);

		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(128, 162, 146, 14);
		PantallaPrincipal.getContentPane().add(progressBar);;

		
		JButton btnEmpezar = new JButton("Empezar");
		btnEmpezar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargar(progressBar);
			}
		});
		
		/*setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panelBarra = new JPanel();
		panelBarra.setLayout(new FlowLayout());
		progressBar = new JProgressBar(0,2000);
		progressBar.setBounds(128, 162, 146, 14);
		progressBar.setValue(num);
		progressBar.setStringPainted(true);
		panelBarra.add(progressBar);
		setContentPane(panelBarra);*/
		
		btnEmpezar.setBounds(165, 206, 109, 23);
		PantallaPrincipal.getContentPane().add(btnEmpezar);
		
		JLabel PrimeraImagen = new JLabel("");
		ImageIcon icon = new ImageIcon("logo.png");
		
		PrimeraImagen.setBounds(61, 29, 329, 102);
		PantallaPrincipal.getContentPane().add(PrimeraImagen);
		PrimeraImagen.setIcon(icon);

	}
	public void cargar(JProgressBar progressBar) {
		
		File archivo = new File("usuarios.txt");
	
		for (int i=1; i<=100; i++) {
			progressBar.setStringPainted(true);
			progressBar.setValue(i);;
			}
		
		if(archivo.exists()) {
			JOptionPane.showMessageDialog(null, "El fichero de texto existe");
			login.getFrame().setVisible(true);
			PantallaPrincipal.dispose();
		}
		else
			JOptionPane.showMessageDialog(null, "No existe el fichero de texto", "PantallaPrincipal", JOptionPane.ERROR_MESSAGE);
	}
	
	public void iterate() {
		  while (num < 2000) {
		  progressBar.setValue(num);
		  try {
		  Thread.sleep(1000);
		  } catch (InterruptedException e) { }
		  num += 95;
		  }
		  
		}
}

