import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuElegir {

	private JFrame Padremenuelegir;
	private JFrame Hijamenuelegir;
	static Login login;

	/**
	 * Launch the application.
	 */
	
	Leccion leccion = new Leccion();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuElegir window = new MenuElegir();
					window.Padremenuelegir.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuElegir() {
		initialize();
		this.login=login;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Padremenuelegir = new JFrame();
		Padremenuelegir.setBounds(100, 100, 450, 300);
		Padremenuelegir.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Padremenuelegir.getContentPane().setLayout(null);
		
		JButton btnLecciones = new JButton("Lecciones");
		btnLecciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PadreMenuElegir();
			}
		});
		btnLecciones.setBounds(132, 70, 128, 34);
		Padremenuelegir.getContentPane().add(btnLecciones);
		
		JButton btnEstadsticas = new JButton("Estad\u00EDsticas");
		btnEstadsticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEstadsticas.setBounds(132, 143, 128, 34);
		Padremenuelegir.getContentPane().add(btnEstadsticas);
		
		JMenuBar menuAcercaDe = new JMenuBar();
		menuAcercaDe.setToolTipText("Acerca de");
		menuAcercaDe.setBounds(10, 11, 107, 21);
		Padremenuelegir.getContentPane().add(menuAcercaDe);
		
		JMenu mnAcercaDe = new JMenu("Acerca de");
		menuAcercaDe.add(mnAcercaDe);
		
		JLabel lblEsteProgramaEst = new JLabel("Este programa est\u00E1 realizado por Diego Momp\u00F3 Redoli el 11 de octubre de 2019 ");
		mnAcercaDe.add(lblEsteProgramaEst);
	}
	public void HijaMenuElegir(Login login) {
		login.getFrame().setVisible(true);
		Hijamenuelegir.dispose();
	}
	public void PadreMenuElegir() {
		leccion.getFrame().setVisible(true);
		Padremenuelegir.dispose();
		
	}
	public JFrame getFrame() {
		return Padremenuelegir;
	}

	public void setFrame(JFrame frame) {
		this.Padremenuelegir = frame;
	}
	
}
