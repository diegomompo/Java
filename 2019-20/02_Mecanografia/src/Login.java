import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Label;
import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Checkbox;
import javax.swing.JPasswordField;

public class Login {

	private JFrame HijaLogin;
	private JFrame PadreLogin;
	static PantallaPrincipal PantallaPrincipal;
	
	MenuElegir menuelegir = new MenuElegir();
	private JPasswordField passwordField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login(PantallaPrincipal);
					window.PadreLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login(PantallaPrincipal PanatallaPrincipal) {
		initialize();
		
		this.PantallaPrincipal=PantallaPrincipal;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		PadreLogin = new JFrame();
		PadreLogin.setBounds(100, 100, 450, 300);
		PadreLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PadreLogin.getContentPane().setLayout(null);
	
		TextField Usuario = new TextField();
		Usuario.setBounds(138, 68, 159, 22);
		PadreLogin.getContentPane().add(Usuario);
		
		passwordField= new JPasswordField();
		passwordField.setBounds(138, 108, 159, 20);
		PadreLogin.add(passwordField);
		
		
		JCheckBox Mostrar = new JCheckBox("Mostrar Contrase\u00F1a");
		Mostrar.setBounds(222, 145, 122, 22);
		PadreLogin.getContentPane().add(Mostrar);
		
		Mostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Mostrar.isSelected()) {
					passwordField.setEchoChar((char) 0);
				}else {
					passwordField.setEchoChar('*');
				}
			}
		});
		
		
		JButton btnIniciarSesin = new JButton("Iniciar sesi\u00F3n");
		btnIniciarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(Usuario.getText().isEmpty() || passwordField.getText().isEmpty()) {
					
					JOptionPane.showMessageDialog(null, "Usuario y/o contrase�a incorrectos", "Login", JOptionPane.ERROR_MESSAGE);
					
				}else {
					
					JOptionPane.showMessageDialog(null, "Ha iniciado sesi�n correctamente");
					LeerUsuarios(Usuario);
					
				}
				
			}
		});
		btnIniciarSesin.setBounds(165, 183, 113, 23);
		PadreLogin.getContentPane().add(btnIniciarSesin);
		
		JMenuBar menuAcercaDe = new JMenuBar();
		menuAcercaDe.setToolTipText("Acerca de");
		menuAcercaDe.setBounds(10, 11, 107, 21);
		PadreLogin.getContentPane().add(menuAcercaDe);
		
		JMenu mnAcercaDe = new JMenu("Acerca de");
		menuAcercaDe.add(mnAcercaDe);
		
		JLabel lblEsteProgramaEst = new JLabel("Este programa est\u00E1 realizado por Diego Momp\u00F3 Redoli el 11 de octubre de 2019 ");
		mnAcercaDe.add(lblEsteProgramaEst);
		
			
	}
	public void LeerUsuarios(TextField Usuario) {
		
		String cadena;
		String text = "";
		String usuario1 = "";
		String usuario2 = "";
		String password1 = "";
		String password2 = "";
		File pf = null;
		
		 pf = new File("usuarios.txt");
		
		 
		try{
			
			
			
			FileReader w = new FileReader(pf);
			BufferedReader bw = new BufferedReader(w);

			
			  while((cadena = bw.readLine())!=null) {
				  int space = cadena.indexOf("");
				  int space2= cadena.indexOf("#");
				  int space3 = cadena.indexOf("*");
				  int space4 = cadena.indexOf("$");
;		           	usuario1 = cadena.substring(0, space2);
		           	password1 = cadena.substring(space2+1, space3);
		           	usuario2 = cadena.substring(space3+1, space4);
		           	password2 = cadena.substring(space4+1, cadena.length());
		           	System.out.println("Usuario: " + usuario1);
		        	System.out.println("Cotrase�a " + password1);
		        	System.out.println("Usuario: " + usuario2);
		        	System.out.println("Cotrase�a " + password2);
		        }
		        
			bw.close();
			w.close();
		}catch(IOException e) {};
		
		if(usuario1.equals(Usuario.getText()) && password1.equals(passwordField.getText()) || usuario2.equals(Usuario.getText()) && password2.equals(passwordField.getText())) {
			  JOptionPane.showMessageDialog(null, "El usuario est� registrado en la base de datos");
			  PadreLogin();
		  }
		else {
			JOptionPane.showMessageDialog(null, "El usuario no est� registrado en la base de datos", "Login", JOptionPane.ERROR_MESSAGE);
		}
		  
	}
		 
	
	public void HijaLogin(PantallaPrincipal PantallaPrincipal) {
		PantallaPrincipal.getFrame().setVisible(true);
		HijaLogin.dispose();
	}
	
	public void PadreLogin() {
		menuelegir.getFrame().setVisible(true);
		PadreLogin.dispose();
	}
	
	public JFrame getFrame() {
		return PadreLogin;
	}

	public void setFrame(JFrame frame) {
		this.PadreLogin = frame;
	}
  }


