import java.awt.EventQueue;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Teclado{

	private JFrame frame;
	Leccion leccion;
	JTextPane textPane = new JTextPane();
	String texto = "";
	String cadena = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Teclado window = new Teclado();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Teclado() {
		initialize();
		this.leccion=leccion;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Scanner sc = new Scanner(System.in);
		
		frame = new JFrame();
		frame.setAlwaysOnTop(true);
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 1463, 858);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		textPane.setEditable(false);
		
		textPane.setBounds(10, 46, 952, 358);
		frame.getContentPane().add(textPane);
		
		JMenuBar menuAcercaDe = new JMenuBar();
		menuAcercaDe.setBorderPainted(false);
		menuAcercaDe.setToolTipText("Acerca de");
		menuAcercaDe.setBounds(10, 11, 107, 21);
		frame.getContentPane().add(menuAcercaDe);
		
		JMenu mnAcercaDe = new JMenu("Acerca de");
		menuAcercaDe.add(mnAcercaDe);
		
		JLabel lblEsteProgramaEst = new JLabel("Este programa est\u00E1 realizado por Diego Momp\u00F3 Redoli el 11 de octubre de 2019 ");
		mnAcercaDe.add(lblEsteProgramaEst);
		
		JButton btnNewButton = new JButton("\\");
		btnNewButton.setEnabled(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(10, 427, 54, 29);
		frame.getContentPane().add(btnNewButton);
		
		JButton button = new JButton("1");
		button.setEnabled(false);
		button.setBounds(65, 427, 62, 29);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("2");
		button_1.setEnabled(false);
		button_1.setBounds(127, 427, 57, 29);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("3");
		button_2.setEnabled(false);
		button_2.setBounds(184, 427, 54, 29);
		frame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("4");
		button_3.setEnabled(false);
		button_3.setBounds(239, 427, 54, 29);
		frame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("5");
		button_4.setEnabled(false);
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_4.setBounds(295, 427, 54, 29);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("6");
		button_5.setEnabled(false);
		button_5.setBounds(353, 427, 54, 29);
		frame.getContentPane().add(button_5);
		
		JButton button_6 = new JButton("7");
		button_6.setEnabled(false);
		button_6.setBounds(408, 427, 57, 29);
		frame.getContentPane().add(button_6);
		
		JButton button_7 = new JButton("8");
		button_7.setEnabled(false);
		button_7.setBounds(468, 427, 57, 29);
		frame.getContentPane().add(button_7);
		
		JButton button_8 = new JButton("9");
		button_8.setEnabled(false);
		button_8.setBounds(529, 427, 57, 29);
		frame.getContentPane().add(button_8);
		
		JButton button_9 = new JButton("0");
		button_9.setEnabled(false);
		button_9.setBounds(587, 427, 57, 29);
		frame.getContentPane().add(button_9);
		
		JButton button_10 = new JButton("?");
		button_10.setEnabled(false);
		button_10.setBounds(647, 427, 57, 29);
		frame.getContentPane().add(button_10);
		
		JButton button_11 = new JButton("\u00BF");
		button_11.setEnabled(false);
		button_11.setBounds(710, 427, 57, 29);
		frame.getContentPane().add(button_11);
		
		JButton button_12 = new JButton("<------");
		button_12.setEnabled(false);
		button_12.setBounds(770, 427, 127, 29);
		frame.getContentPane().add(button_12);
		
		JButton button_13 = new JButton("|<---");
		button_13.setEnabled(false);
		button_13.setBounds(7, 461, 70, 29);
		frame.getContentPane().add(button_13);
		
		JButton btnShift = new JButton("MAYUS");
		btnShift.setEnabled(false);
		btnShift.setBounds(10, 501, 75, 29);
		frame.getContentPane().add(btnShift);
		
		JButton btnY = new JButton("Y");
		btnY.setEnabled(false);
		btnY.setBounds(363, 461, 54, 29);
		frame.getContentPane().add(btnY);
		
		JButton btnU = new JButton("U");
		btnU.setEnabled(false);
		btnU.setBounds(422, 461, 57, 29);
		frame.getContentPane().add(btnU);
		
		JButton btnE = new JButton("E");
		btnE.setEnabled(false);
		btnE.setBounds(194, 461, 54, 29);
		frame.getContentPane().add(btnE);
		
		JButton btnU_1 = new JButton("O");
		btnU_1.setEnabled(false);
		btnU_1.setBounds(539, 461, 54, 29);
		frame.getContentPane().add(btnU_1);
		
		JButton btnT = new JButton("T");
		btnT.setEnabled(false);
		btnT.setBounds(305, 461, 54, 29);
		frame.getContentPane().add(btnT);
		
		JButton btnP = new JButton("[");
		btnP.setEnabled(false);
		btnP.setBounds(657, 461, 57, 29);
		frame.getContentPane().add(btnP);
		
		JButton btnQ = new JButton("Q");
		btnQ.setEnabled(false);
		btnQ.setBounds(79, 461, 62, 29);
		frame.getContentPane().add(btnQ);
		
		JButton btnO = new JButton("I");
		btnO.setEnabled(false);
		btnO.setBounds(483, 461, 57, 29);
		frame.getContentPane().add(btnO);
		
		JButton btnO_1 = new JButton("P");
		btnO_1.setEnabled(false);
		btnO_1.setBounds(597, 461, 57, 29);
		frame.getContentPane().add(btnO_1);
		
		JButton button_23 = new JButton("]");
		button_23.setEnabled(false);
		button_23.setBounds(720, 461, 57, 29);
		frame.getContentPane().add(button_23);
		
		JButton btnR = new JButton("R");
		btnR.setEnabled(false);
		btnR.setBounds(249, 461, 54, 29);
		frame.getContentPane().add(btnR);
		
		JButton btnW = new JButton("W");
		btnW.setEnabled(false);
		btnW.setBounds(145, 461, 57, 29);
		frame.getContentPane().add(btnW);
		
		JButton btnEnter = new JButton("ENTER");
		btnEnter.setEnabled(false);
		btnEnter.setBounds(808, 461, 86, 69);
		frame.getContentPane().add(btnEnter);
		
		JButton btnA = new JButton("A");
		btnA.setEnabled(false);
		btnA.setBounds(87, 501, 62, 29);
		frame.getContentPane().add(btnA);
		
		JButton btnS = new JButton("S");
		btnS.setEnabled(false);
		btnS.setBounds(155, 501, 49, 29);
		frame.getContentPane().add(btnS);
		
		JButton btnD = new JButton("D");
		btnD.setEnabled(false);
		btnD.setBounds(208, 501, 49, 29);
		frame.getContentPane().add(btnD);
		
		JButton btnF = new JButton("F");
		btnF.setEnabled(false);
		btnF.setBounds(259, 501, 54, 29);
		frame.getContentPane().add(btnF);
		
		JButton btnG = new JButton("G");
		btnG.setEnabled(false);
		btnG.setBounds(315, 501, 54, 29);
		frame.getContentPane().add(btnG);
		
		JButton btnH = new JButton("H");
		btnH.setEnabled(false);
		btnH.setBounds(373, 501, 52, 29);
		frame.getContentPane().add(btnH);
		
		JButton btnJ = new JButton("J");
		btnJ.setEnabled(false);
		btnJ.setBounds(432, 501, 54, 29);
		frame.getContentPane().add(btnJ);
		
		JButton btnK = new JButton("K");
		btnK.setEnabled(false);
		btnK.setBounds(493, 501, 49, 29);
		frame.getContentPane().add(btnK);
		
		JButton btnL = new JButton("L");
		btnL.setEnabled(false);
		btnL.setBounds(546, 501, 54, 29);
		frame.getContentPane().add(btnL);
		
		JButton button_25 = new JButton("\u00D1");
		button_25.setEnabled(false);
		button_25.setBounds(604, 501, 54, 29);
		frame.getContentPane().add(button_25);
		
		JButton button_26 = new JButton("{");
		button_26.setEnabled(false);
		button_26.setBounds(664, 501, 62, 29);
		frame.getContentPane().add(button_26);
		
		JButton button_27 = new JButton("}");
		button_27.setEnabled(false);
		button_27.setBounds(730, 501, 68, 29);
		frame.getContentPane().add(button_27);
		
		JButton btnSfhit = new JButton("SHIFT");
		btnSfhit.setEnabled(false);
		btnSfhit.setBounds(10, 541, 67, 29);
		frame.getContentPane().add(btnSfhit);
		
		JButton button_15 = new JButton("<");
		button_15.setEnabled(false);
		button_15.setBounds(79, 541, 48, 29);
		frame.getContentPane().add(button_15);
		
		JButton btnZ = new JButton("Z");
		btnZ.setEnabled(false);
		btnZ.setBounds(130, 541, 54, 29);
		frame.getContentPane().add(btnZ);
		
		JButton btnX = new JButton("X");
		btnX.setEnabled(false);
		btnX.setBounds(184, 541, 54, 29);
		frame.getContentPane().add(btnX);
		
		JButton btnC = new JButton("C");
		btnC.setEnabled(false);
		btnC.setBounds(239, 541, 54, 29);
		frame.getContentPane().add(btnC);
		
		JButton btnV = new JButton("V");
		btnV.setEnabled(false);
		btnV.setBounds(295, 541, 54, 29);
		frame.getContentPane().add(btnV);
		
		JButton btnB = new JButton("B");
		btnB.setEnabled(false);
		btnB.setBounds(353, 541, 54, 29);
		frame.getContentPane().add(btnB);
		
		JButton btnN = new JButton("N");
		btnN.setEnabled(false);
		btnN.setBounds(408, 541, 57, 29);
		frame.getContentPane().add(btnN);
		
		JButton btnM = new JButton("M");
		btnM.setEnabled(false);
		btnM.setBounds(468, 541, 54, 29);
		frame.getContentPane().add(btnM);
		
		JButton button_24 = new JButton(",");
		button_24.setEnabled(false);
		button_24.setBounds(526, 541, 49, 29);
		frame.getContentPane().add(button_24);
		
		JButton button_28 = new JButton(".");
		button_28.setEnabled(false);
		button_28.setBounds(576, 541, 62, 29);
		frame.getContentPane().add(button_28);
		
		JButton button_29 = new JButton("-");
		button_29.setEnabled(false);
		button_29.setBounds(639, 541, 75, 29);
		frame.getContentPane().add(button_29);
		
		JButton btnShift_1 = new JButton("SHIFT");
		btnShift_1.setEnabled(false);
		btnShift_1.setBounds(720, 541, 177, 29);
		frame.getContentPane().add(btnShift_1);
		
		JButton btnControl = new JButton("CRTL");
		btnControl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnControl.setEnabled(false);
		btnControl.setBounds(10, 581, 81, 29);
		frame.getContentPane().add(btnControl);
		
		JButton btnInicio = new JButton("IN");
		btnInicio.setEnabled(false);
		btnInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInicio.setBounds(95, 581, 46, 29);
		frame.getContentPane().add(btnInicio);
		
		JButton btnAlt = new JButton("Alt");
		btnAlt.setEnabled(false);
		btnAlt.setBounds(145, 581, 70, 29);
		frame.getContentPane().add(btnAlt);
		
		JButton button_21 = new JButton("|");
		button_21.setEnabled(false);
		button_21.setBounds(720, 581, 70, 29);
		frame.getContentPane().add(button_21);
		
		JButton btnAltGr = new JButton("Alt Gr");
		btnAltGr.setEnabled(false);
		btnAltGr.setBounds(571, 581, 67, 29);
		frame.getContentPane().add(btnAltGr);
		
		JButton button_14 = new JButton("CONTROL");
		button_14.setEnabled(false);
		button_14.setBounds(800, 581, 97, 29);
		frame.getContentPane().add(button_14);
		
		JButton button_16 = new JButton("IN");
		button_16.setEnabled(false);
		button_16.setBounds(639, 581, 74, 29);
		frame.getContentPane().add(button_16);
		
		JButton espacio= new JButton("");
		espacio.setEnabled(false);
		frame.getContentPane().add(espacio);
		
		espacio.setBounds(225, 581, 341, 29);
		frame.getContentPane().add(espacio);
	}
				
	public void MostrarLeccion() {
		String texto = "";
		String cadena = "";
		File pf = null;
		
		pf = new File("lecciones.txt");
		
		try { 
			FileReader r = new FileReader(pf);
			BufferedReader br = new BufferedReader(r);
		
			/*while((texto = br.readLine()) != null) {
				int space = texto.indexOf("#");
				int space2= texto.indexOf("*");
				cadena = texto.substring(space+1, space2);
				System.out.println(cadena);
				textPane.setText(cadena);
			}*/
			
			if((texto = br.readLine()) != null) {
				int space = texto.indexOf("#");
				int space2= texto.indexOf("*");
				cadena = texto.substring(space+1, space2);
				System.out.println(cadena);
				textPane.setText(cadena);
			}
			
		}catch(IOException e) {};
	}
	public void MostrarLeccion2() {
		String texto = "";
		String cadena = "";
		File pf = null;
		
		pf = new File("lecciones.txt");
		
		try {
			FileReader r = new FileReader(pf);
			BufferedReader br = new BufferedReader(r);
			
			/*while((texto = br.readLine()) != null) {
				int space = texto.indexOf("@");
				int space2= texto.indexOf("$");
				cadena = texto.substring(space+1, space2);
				System.out.println(cadena);
				textPane.setText(cadena);
			}*/
			
			if((texto = br.readLine()) != null) {
				int space2 = texto.indexOf("@");
				cadena = texto.substring(space2+1, texto.length()-1);
				System.out.println(cadena);
				textPane.setText(cadena);
				
				br.close();
			}
			
		}catch(IOException e) {};
	}
	
	
	
	public void HijaTeclado(Leccion Leccion) {
		leccion.getFrame().setVisible(true);
		frame.dispose();
	}
	public Window getFrame() {
		// TODO Auto-generated method stub
		return frame;
	}

}
