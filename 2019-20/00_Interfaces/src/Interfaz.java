import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ButtonGroup;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;


public class Interfaz {

	private JFrame frame;
	private JTextField txthaAparecidoCristiano;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txthaAparecidoCristiano = new JTextField();
		txthaAparecidoCristiano.setBounds(10, 32, 226, 20);
		frame.getContentPane().add(txthaAparecidoCristiano);
		txthaAparecidoCristiano.setText("\u00BFHa aparecido Cristiano Ronaldo en el partido?");
		txthaAparecidoCristiano.setColumns(10);
		
		JRadioButton botonSI = new JRadioButton("SI");
		buttonGroup.add(botonSI);
		botonSI.setBounds(20, 59, 109, 23);
		frame.getContentPane().add(botonSI);
		
		JRadioButton BotonNO = new JRadioButton("NO");
		buttonGroup.add(BotonNO);
		BotonNO.setBounds(140, 59, 109, 23);
		frame.getContentPane().add(BotonNO);
		
		JButton EnviarResultado = new JButton("ENVIAR");
		EnviarResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(BotonNO.isSelected()) {
					JOptionPane.showMessageDialog(null, "Reproduciendo audio");
					ReproducirSonido("2.wav");
					JOptionPane.showMessageDialog(null, "Audio Reproducido");
				}
			
			}
		});
		EnviarResultado.setBounds(10, 110, 89, 23);
		frame.getContentPane().add(EnviarResultado);
		
		JButton btnPausa = new JButton("PAUSA");
		btnPausa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JOptionPane.showMessageDialog(null, "Pausando audio");
				PausarSonido("2.wav");
				JOptionPane.showMessageDialog(null, "Audio Pausado");
			}
		});
		btnPausa.setBounds(140, 110, 89, 23);
		frame.getContentPane().add(btnPausa);
	}
	public void ReproducirSonido(String nombreSonido){
	       try {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(nombreSonido).getAbsoluteFile());
	        Clip clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.start();
	       } catch(UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
	         System.out.println("Error al reproducir el sonido.");
	       }
	     }
	public void PausarSonido(String nombreSonido){
	       try {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(nombreSonido).getAbsoluteFile());
	        Clip clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.stop();
	       } catch(UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
	         System.out.println("Error al reproducir el sonido.");
	       }
	     }
}
