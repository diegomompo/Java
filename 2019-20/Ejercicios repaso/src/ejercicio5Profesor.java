
public class ejercicio5Profesor extends ejercicio5Persona{
	
	int n_asignaturas;
	boolean tutor;
	
	public ejercicio5Profesor(String dni, String nombre, String apellidos, double salario, int n_asignaturas,
			boolean tutor) {
		super(dni, nombre, apellidos, salario);
		this.n_asignaturas = n_asignaturas;
		this.tutor = tutor;
	}

	public int getN_asignaturas() {
		return n_asignaturas;
	}

	public void setN_asignaturas(int n_asignaturas) {
		this.n_asignaturas = n_asignaturas;
	}

	public boolean isTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}

	@Override
	public String toString() {
		return "ejercicio5Profesor [DNI=" + DNI + ", nombre="
				+ nombre + ", apellidos=" + apellidos + ", salario=" + salario + ", n_asignaturas=" + n_asignaturas + ", tutor=" + tutor + "]";
	}
	
	
	
	

	


	
}
