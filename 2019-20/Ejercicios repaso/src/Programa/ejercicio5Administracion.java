package Programa;

public class ejercicio5Administracion extends ejercicio5Persona{

	char sexo;
	int n_horasextra;
	
	public ejercicio5Administracion(String dni, String nombre, String apellidos, double salario, char sexo,
			int n_horasextra) {
		super(dni, nombre, apellidos, salario);
		this.sexo = sexo;
		this.n_horasextra = n_horasextra;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public int getN_horasextra() {
		return n_horasextra;
	}

	public void setN_horasextra(int n_horasextra) {
		this.n_horasextra = n_horasextra;
	}

	@Override
	public String toString() {
		return "ejercicio5Administracion [DNI=" + DNI + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario="
				+ salario + ", sexo=" + sexo + ", n_horasextra=" + n_horasextra + "]";
	}
	
	
	
	
	
	
}
