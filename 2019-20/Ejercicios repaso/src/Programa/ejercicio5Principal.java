package Programa;

public class ejercicio5Principal {

	public static void main(String[] args) {
		
		ejercicio5Profesor profe = new ejercicio5Profesor("12345678R", "Monica", "Mompo", 3000, 3, false);
		ejercicio5Administracion admin = new ejercicio5Administracion("89343890D", "Maru", "Mompo", 4000, 'f', 0);
		ejercicio5Directivo direc = new ejercicio5Directivo("41534498U", "Diego", "Mompo", 5000, false, 'm');
		
		System.out.println(profe);
		System.out.println(admin);
		System.out.println(direc);

	}

}
