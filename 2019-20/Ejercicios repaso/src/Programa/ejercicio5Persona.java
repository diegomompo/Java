package Programa;
public class ejercicio5Persona {
	
	protected String DNI;
	protected String nombre;
	protected String apellidos;
	protected double salario;
	
	public ejercicio5Persona(String dni, String nombre, String apellidos, double salario) {
		DNI = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
	}

	protected String getDNI() {
		return DNI;
	}

	protected void setDNI(String dNI) {
		DNI = dNI;
	}

	protected String getNombre() {
		return nombre;
	}

	protected void setNombre(String nombre) {
		this.nombre = nombre;
	}

	protected String getApellidos() {
		return apellidos;
	}

	protected void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	protected double getSalario() {
		return salario;
	}

	protected void setSalario(double salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "ejercicio5Persona [DNI=" + DNI + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario="
				+ salario + "]";
	}
	
	
}
