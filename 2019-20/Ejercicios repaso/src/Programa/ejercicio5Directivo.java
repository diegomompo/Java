package Programa;

public class ejercicio5Directivo extends ejercicio5Persona {
	
	boolean salesiano;
	char turno;
	public ejercicio5Directivo(String dni, String nombre, String apellidos, double salario, boolean salesiano,
			char turno) {
		super(dni, nombre, apellidos, salario);
		this.salesiano = salesiano;
		this.turno = turno;
	}
	public boolean isSalesiano() {
		return salesiano;
	}
	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}
	public char getTurno() {
		return turno;
	}
	public void setTurno(char turno) {
		this.turno = turno;
	}
	@Override
	public String toString() {
		return "ejercicio5Directivo [DNI=" + DNI + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario="
				+ salario + ", salesiano=" + salesiano + ", turno=" + turno + "]";
	}
	
	
}
