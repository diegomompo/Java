import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int numero;
		
		System.out.println("Introduce un n�mero");
		
		numero = sc.nextInt();
		
		ejercicio4factorial f = new ejercicio4factorial();
		
		System.out.println("El n�mero factorial de " + numero + " es " + f.factorial(numero) + ".");

	}
	
}

// ---------------------------------------------------------------------------------------------------------------------------------------
/* 

public class ejercicio4factorial {

public static int factorial(int numero) {
	if(numero ==  0) {
		return 1;
	}else
		return numero * factorial(numero-1);
}

}
*/